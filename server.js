'use strict';

const path = require('path');
const express = require('express');
const controllers = require('./controllers');
const app = express();

app.get('/api/artists', function(req, res, next) {
	let { username, minListens = 1, numArtists = 1 } = req.query;
	if (!req.query.username) {
		let err = new Error('Username required.');
		err.status = 422;
		return next(err);
	}

	controllers.randomArtists({ username, minListens, numArtists })
		.then(function(artists) {
			res.json(artists);
		})
		.catch(next);
});

app.use('/', express.static(path.join(__dirname, 'public')));

app.use(function pageNotFound(req, res) {
	return res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

app.use(function(err, req, res, next) {
	let status = err.status || 500;
	let info = {
		error: err.message,
		err,
		status
	};

	console.error(err);

	if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'prod') {
		return res.status(500).json({error: 'Something went wrong'});
	} else {
		return res.status(status).json(info);
	}
});

module.exports = {
	app
};
