'use strict';

if (process.env.NODE_ENV !== 'production') {
	require('dotenv').load();
}
const { app } = require('./server');
const PORT = process.env.PORT;

app.listen(PORT, function() {
	console.log(`Server listening on port ${PORT}`);
});
