## Motivation

Sometimes I want to listen to music, but don't have any particular playlist/genre/artist in mind. This site will give you a random artist, given the constraint that you (or someone else) have listened to and scrobbled them before.

For this project, I used angular for the first time, and deployed to Heroku for only the second time, so there is some sloppiness in the code and commits. There is also no styling. I may or may not address these issues, as the project serves my purposes well as-is.

## Installing and Running
Running `npm install` in the top level of this project will install the dependencies for the backend and the frontend, as well as building the angular project, which will store its output in the top-level `public/` directory. You can then use `npm start` to start the server and serve the application.