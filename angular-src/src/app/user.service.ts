import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class UserService {
    private _username = new BehaviorSubject<string>('');
    username = this._username.asObservable();

    updateUser(newUsername: string) {
        this._username.next(newUsername);
    }
}