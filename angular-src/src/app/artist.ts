export interface Artist {
	name: string;
	listeners: number;
	images: string[];
}
