import { Component, Input } from '@angular/core';
import { Artist } from '../artist';
import { UserService } from '../user.service';

@Component({
	selector: 'app-artist',
	templateUrl: './artist.component.html',
	styleUrls: ['./artist.component.css']
})
export class ArtistComponent {
	constructor(private userService: UserService) {
		this.userService.username.subscribe(username => {
			this.username = username;
		});
	}

	private username: string = '';
	
	@Input() artist: Artist;

	get imgUrl(): string {
		// Images are returned smallest first
		if (this.artist && this.artist.images) {
			return this.artist.images[this.artist.images.length - 1];
		}

		return '';
	}

	get libraryUrl(): string {
		if (!this.artist || !this.artist.name || !this.username) {
			return '';
		}

		return `https://www.last.fm/user/${this.username}/library/music/${this.artist.name.replace(/ /g, '+')}`;
	}

	get isAnError(): boolean {
		return this.artist.listeners === -1 && this.artist.name === 'Error';
	}

}
