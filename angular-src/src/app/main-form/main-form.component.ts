import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';
import { ArtistsService } from '../artists.service';
import { Artist } from '../artist';
import { UserService } from '../user.service';

@Component({
	selector: 'app-main-form',
	templateUrl: './main-form.component.html',
	styleUrls: ['./main-form.component.css']
})
export class MainFormComponent implements OnInit, OnDestroy {
	username: string;
	minListens = 1;
	@Input() artistSubject: Subject<Artist>;
	private subscription: Subscription;
	private queryUsername: string;

	constructor(
		private artistsService: ArtistsService,
		private userService: UserService,
		private route: ActivatedRoute,
		private router: Router
	) { }

	ngOnInit() {
		this.route.queryParamMap
			.map(param => param.get('user'))
			.subscribe(username => {
				this.username = username;
				this.queryUsername = username;
			}, null, null);
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}

	onSubmit(): void {
		this.subscription = this.artistsService.getArtist(this.username, this.minListens)
			.subscribe(this.onNext.bind(this), this.onError, null);

		if (this.queryUsername !== this.username) {
			this.router.navigate([], { queryParams: { 'user': this.username } });
			this.queryUsername = this.username;
		}
	}

	private onNext(artist: Artist): void {
		this.artistSubject.next(artist);
		this.userService.updateUser(this.username);
	}

	private onError(error: any): void {
		alert('Sorry, something went wrong');
	}

}
