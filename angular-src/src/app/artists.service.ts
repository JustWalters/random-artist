import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, finalize, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { Artist } from './artist';

@Injectable()
export class ArtistsService {
	private artistsUrl = 'api/artists';

	constructor(private http: HttpClient) { }

	getArtist(username: string, minListens: number): Observable<Artist> {
		const date = new Date().toISOString();
		const loaderId = `getting-artist-${date}`;
		Loader.start(loaderId);

		return this.http.get<Artist[]>(this.artistsUrl, {
			params: {
				username,
				minListens: String(minListens),
				numArtists: '1'
			}
		})
		.pipe(
			map(artists => artists[0]),
			finalize(() => Loader.stop(loaderId)),
			catchError(this.handleError('getArtist', {name: 'Error', listeners: -1} as Artist))
		);
	}

	/**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
	private handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			// TODO: send the error to remote logging infrastructure
			// console.error(error); // log to console instead

			// TODO: better job of transforming error for user consumption

			// Let the app keep running by returning an empty result.
			return of(result as T);
		};
	}

}
