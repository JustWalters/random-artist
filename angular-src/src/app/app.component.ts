import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Artist } from './artist';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
	artists: Artist[] = [];
	artistSubject = new Subject<Artist>();

	ngOnInit() {
		this.artistSubject.subscribe(artist => this.artists.unshift(artist));
	}

	ngOnDestroy() {
		this.artistSubject.unsubscribe();
	}
}
