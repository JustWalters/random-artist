import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MainFormComponent } from './main-form/main-form.component';
import { ArtistsService } from './artists.service';
import { ArtistComponent } from './artist/artist.component';
import { UserService } from './user.service';

const appRoutes: Routes = [
	{ path: '', pathMatch: 'full', component: AppComponent },
	{ path: '**', redirectTo: '' }
];

@NgModule({
	declarations: [
		AppComponent,
		MainFormComponent,
		ArtistComponent
	],
	imports: [
		HttpClientModule,
		BrowserModule,
		FormsModule,
		RouterModule.forRoot(appRoutes)
	],
	providers: [ArtistsService, UserService],
	bootstrap: [AppComponent]
})
export class AppModule { }
