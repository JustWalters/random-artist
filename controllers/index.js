'use strict';

const LAST_FM_API_KEY = process.env.LAST_FM_API_KEY;
const RAPID_API_KEY = process.env.RAPID_API_KEY;
const http = require('https');
const LastFM = require('last-fm');
const lastfm = new LastFM(LAST_FM_API_KEY, {
	userAgent: 'Random Artist/1.1.0 (https://random-artist.uc.r.appspot.com/)',
});

function randomArtists(opts) {
	let { username, minListens = 1, numArtists = 1 } = opts;
	if (minListens < 1) minListens = 1;
	if (numArtists > 10) numArtists = 10;
	return new Promise((resolve, reject) => {
		lastfm.libraryArtists({ user: username, limit: numArtists }, (err, data) => {
			if (err) return reject(err);

			if (!data || !data.meta || !data.meta.total) return reject(new Error('No artists found'));
			let total = data.meta.total;

			return getRandomPageWithMinListens({ maxPage: total, user: username, minListens })
				.then(getImage)
				.then(resolve)
				.catch(reject);
		});
	});
}

async function imageApiSearch(searchString) {
	const query = encodeURIComponent(`${searchString} musical artist`);
	const options = {
		method: "GET",
		hostname: "real-time-image-search.p.rapidapi.com",
		port: null,
		path: `/search?query=${query}&size=medium&type=photo&region=us`,
		headers: {
			"X-RapidAPI-Key": RAPID_API_KEY,
			"X-RapidAPI-Host": "real-time-image-search.p.rapidapi.com",
		},
	};
	http.request(options);

	return new Promise((resolve, reject) => {
		const req = http.request(options, function (res) {
			const chunks = [];

			res.on("data", function (chunk) {
				chunks.push(chunk);
			});

			res.on("end", function () {
				try {
					const body = Buffer.concat(chunks);
					console.log(body.toString());
					resolve(JSON.parse(body.toString()));
				} catch (err) {
					reject(err);
				}
			});

			res.on("error", function (error) {
				reject(error);
			});
		});

		req.end();
	}).then((response) => {
		if (response && response.data && response.data.length) {
			return response.data.map((result) => result.url).filter((url) => url);
		}
		return null;
	});
}

async function getImage(artists) {
	const artistsWithImages = await Promise.all(
		artists.map(async (artist) => {
			try {
				const imageUrls = await imageApiSearch(artist.name);
				console.log("IR", imageUrls);

				if (imageUrls && imageUrls[0]) {
					return {
						...artist,
						images: imageUrls,
					};
				} else {
					console.log(`No image found for ${artist.name}`, imageUrls);
					return artist;
				}
			} catch (err) {
				console.error(`Error getting image for ${artist.name}`, err);
				return artist;
			}
		})
	);

	return artistsWithImages;
}

async function getRandomPageWithMinListens(opts) {
	let { maxPage, user, minListens } = opts;
	let data = await getRandomPage({ maxPage, user });
	let artist = data.result[0];
	if (artist.listeners >= minListens) return data.result;

	// Should only check earlier pages because results sorted by listens
	let curPage = data.meta.page;
	return getRandomPageWithMinListens({ maxPage: curPage, user, minListens });
}

function getRandomPage(opts) {
	let { maxPage, user } = opts;
	return new Promise((resolve, reject) => {
		let page = random(maxPage);
		console.log("GETTING RANDOM LESS THAN", maxPage, "so", page);
		lastfm.libraryArtists({ page, user, limit: 1 }, (err, data) => {
			if (err) return reject(err);
			return resolve(data);
		});
	});
}

function random(max) {
	return Math.floor(Math.random() * max);
}

module.exports = {
	randomArtists,
};
